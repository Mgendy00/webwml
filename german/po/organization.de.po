# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004, 2005, 2006, 2010, 2012, 2016, 2017, 2018, 2019.
# Helge Kreutzmann <debian@helgefjell.de>, 2007, 2009, 2011.
# Gerfried Fuchs <rhonda@debian.at>, 2002, 2003, 2004, 2008.
# Jens Seidel <tux-master@web.de>, 2004, 2006.
# Holger Wansing <linux@wansing-online.de>, 2011 - 2015, 2020.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2020-02-16 20:10+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "Delegations-Mail"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "Berufungs-Mail"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegiert"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegiert"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"female\"/>delegiert"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"female\"/>delegiert"

#: ../../english/intro/organization.data:24
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"female\"/>delegiert"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"female\"/>delegiert"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "aktuell"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "Mitglied"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "Verwalter"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Release-Manager für Stable"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "Berater"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "Vorsitzender"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "Assistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "Schriftführer"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "Vertreter"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "Funktion"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Direktoren"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribution"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr "Kommunikation und Outreach"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr "Datenschutz-Team"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "Öffentlichkeitsarbeits-Team"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr "Mitgliedschaft in anderen Organisationen"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Unterstützung und Infrastruktur"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Projektleiter"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Technischer Ausschuss"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Schriftführer"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Entwicklungs-Projekte"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP-Archive"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP-Master"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP-Mitarbeiter"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "FTP-Berater"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Backports-Team"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Release-Verwaltung"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Release-Team"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Qualitätssicherung"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Installationssystem-Team"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr "Debian-Live-Team"

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Veröffentlichungshinweise"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "CD-Images"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produktion"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr "Cloud-Team"

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Autobuilding-Infrastruktur"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Wanna-build-Team"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Buildd-Administration"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Dokumentation"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Liste der Arbeit-bedürfenden und voraussichtlichen Pakete"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Pressekontakt"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Webseiten"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "Outreach"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Debian-Women-Projekt"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Veranstaltungen"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "DebConf-Ausschuss"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Partner-Programm"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Hardware-Spenden-Koordination"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr "SchoolForge"

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Fehlerdatenbank"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Mailinglisten-Verwaltung und -Archive"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "Empfang (Front desk) für neue Mitglieder"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Debian-Konten-Verwalter"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Um eine private Nachricht an alle DAMs zu schicken, verwenden Sie den GPG-"
"Schlüssel 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Schlüsselring-Verwalter (PGP und GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Sicherheitsteam"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Policy"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "System-Administration"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Diese Adresse soll benutzt werden, wenn Probleme mit einer von Debians "
"Maschinen aufgetreten sind, auch wenn es sich um Passwort-Probleme handelt "
"oder wenn neue Pakete installiert werden sollen."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Falls Sie Hardware-Probleme mit Debian-Rechnern bemerken, prüfen Sie bitte "
"die Übersicht der <a href=\"https://db.debian.org/machines.cgi\">Debian-"
"Rechner</a>, sie sollte Administrator-Informationen für jede Maschine "
"enthalten."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP-Entwickler-Verzeichnis-Verwalter"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Spiegel"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "DNS-Verwalter"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Paketverfolgungs-System"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr "Schatzmeister"

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Anfragen zur Verwendung der <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">Handelsmarken</a>"

#: ../../english/intro/organization.data:392
msgid "Salsa administrators"
msgstr "Salsa-Administratoren"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends (angepasste Debian-Distributionen)"

#~ msgid "Individual Packages"
#~ msgstr "Individuelle Pakete"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian für Kinder von 1 bis 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian für die medizinische Praxis und Forschung"

#~ msgid "Debian for education"
#~ msgstr "Debian für Bildung"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian in Rechtsanwaltskanzleien"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian für Behinderte"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian für die Wissenschaft und zugehörige Forschung"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian für Astronomie"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth-Administratoren"

#~ msgid "Publicity"
#~ msgstr "Öffentlichkeitsarbeit"

#~ msgid "Bits from Debian"
#~ msgstr "Neuigkeiten von Debian"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Schlüsselring-Betreuer für Debian-Betreuer (DM)"

#~ msgid "DebConf chairs"
#~ msgstr "DebConf-Leitung"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (nicht aktiv: wurde nicht mit Squeeze veröffentlicht)"

#~ msgid "Vendors"
#~ msgstr "Distributoren"

#~ msgid "Handhelds"
#~ msgstr "Handheldcomputer"

#~ msgid "Marketing Team"
#~ msgstr "Marketing-Team"

#~ msgid "Key Signing Coordination"
#~ msgstr "Schlüssel-Signierungs-Koordination"

#~ msgid "Volatile Team"
#~ msgstr "Volatile-Team"

#~ msgid "Security Audit Project"
#~ msgstr "Sicherheits-Audit-Projekt"

#~ msgid "Testing Security Team"
#~ msgstr "Testing-Sicherheitsteam"

#~ msgid "current Debian Project Leader"
#~ msgstr "derzeitiger Debian-Projektleiter"

#~ msgid "Live System Team"
#~ msgstr "Live-System-Team"

#~ msgid "Auditor"
#~ msgstr "Kassenprüfer"

#~ msgid "Anti-harassment"
#~ msgstr "Gegen Mobbing/Belästigung"

#~ msgid "User support"
#~ msgstr "Benutzer-Unterstützung"

#~ msgid "Embedded systems"
#~ msgstr "Eingebettete Systeme"

#~ msgid "Firewalls"
#~ msgstr "Firewalls"

#~ msgid "Laptops"
#~ msgstr "Laptops"

#~ msgid "Special Configurations"
#~ msgstr "Spezielle Konfigurationen"

#~ msgid "Ports"
#~ msgstr "Portierungen"

#~ msgid "CD Vendors Page"
#~ msgstr "CD-Distributoren-Seite"

#~ msgid "Consultants Page"
#~ msgstr "Beraterseite"
