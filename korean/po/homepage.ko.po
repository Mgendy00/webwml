# Sangdo Jun <sebuls@gmail.com>, 2020
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Sangdo Jun <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "범용 운영체제"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "DC19 그룹 사진"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "데브컨프19 그룹 사진"

#: ../../english/index.def:19
msgid "Mini DebConf Hamburg 2018"
msgstr "미니 데브컨프 함부르크 2018"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Hamburg 2018"
msgstr "함부르크 2018 미니데브컨프 단체 사진"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr ""

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr ""

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "데비안은 스위스 군용 칼 같습니다"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "사람들이 데비안과 함께 재미있게 지냅니다"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr ""

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr ""

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "블로그"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "마이크로뉴스"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "데비안에서 온 마이크로뉴스"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "플래닛"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "데비안 플래닛"
