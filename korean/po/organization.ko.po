# Debian organizition
# Copyright (C) YEAR Free Software Foundation, Inc.
# Sangdo Jun <sebuls@gmail.com>, 2020
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-22 05:43+0900\n"
"Last-Translator: Sangdo Jun <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "위임 메일"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr ""

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr ""

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "안정 릴리스 관리자"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr ""

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr ""

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr ""

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr ""

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr ""

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr ""

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "리더"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "기술 위원회"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "사무국장"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "개발 프로젝트"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP 저장소"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr ""

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "백포트"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "백포트 팀"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "릴리스 관리"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "릴리스 팀"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "품질 보증"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "설치 시스템 팀"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr "데비안 라이브 팀"

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "릴리스 노트"

#: ../../english/intro/organization.data:152
msgid "CD/DVD/USB Images"
msgstr "CD/DVD/USB Images"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr ""

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr ""

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr ""

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr ""

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "문서"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr ""

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr ""

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "웹 페이지"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "데비안 여성 프로젝트"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr "커뮤니티"

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "행사"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "Debconf 기술위원회"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr ""

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "버그 추적 시스템"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "개발자 계정 관리자"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "키링 관리자(PGP와 GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "보안 팀"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "정책"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "시스템 관리"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr ""

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "미러"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "DNS 관리자"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "패키지 추적 시스템"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:392
msgid "Salsa administrators"
msgstr "Salsa 관리자"
