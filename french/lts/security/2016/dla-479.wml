#use wml::debian::translation-check translation="fce40adff1381ed16a3e5ebae7ad1ff8fcbbb739" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour de sécurité corrige un certain nombre de problèmes de
sécurité dans Xen dans Wheezy.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.1.6.1-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-2752">CVE-2015-2752</a>

<p>L'hyperappel XEN_DOMCTL_memory_mapping dans Xen, versions 3.2.x à 4.5.x,
lors de l'utilisation d'un périphérique PCI intermédiaire, n'est pas
préemptable. Cela permet à des utilisateurs locaux de domaine x86 HVM de
provoquer un déni de service (consommation excessive du processeur de
l'hôte) à l'aide d'une requête contrefaite au modèle de périphérique
(qemu-dm).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-2756">CVE-2015-2756</a>

<p>QEMU, tel qu'utilisé dans Xen versions 3.3.x à 4.5.x, ne restreint pas
correctement l'accès aux registres de commande PCI, ce qui pourrait
permettre à des clients HVM locaux de provoquer un déni de service
(interruption non masquable et plantage de l'hôte) en désactivant le
décodage (1) de la mémoire ou (2) des E/S d'un périphérique PCI Express et en
accédant ensuite au périphérique, ce qui déclenche une réponse « Unsupported
Request » (UR).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5165">CVE-2015-5165</a>

<p>L'émulation du mode offload de C+ du modèle de carte réseau RTL8139 dans
QEMU, telle qu'utilisée dans les versions 4.5.x et antérieures de Xen,
permet à des attaquants distants la lecture de la mémoire de tas du
processus au moyen de vecteurs non précisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5307">CVE-2015-5307</a>

<p>Le sous-système KVM du noyau jusqu'à 4.2.6 et Xen, des versions 4.3.x
à 4.6.x, permet aux utilisateurs du système d'exploitation client de
provoquer un déni de service (panique ou blocage du système hôte) en
déclenchant plusieurs exceptions de « vérifications d'alignement » (#AC),
liées à svm.c et vmx.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7969">CVE-2015-7969</a>

<p>Plusieurs fuites de mémoire dans Xen 4.0 jusqu'à 4.6.x permettent à des
administrateurs ou domaines de clients locaux avec certaines permissions de
provoquer un déni de service (consommation de mémoire) à l'aide d'un grand
nombre de <q>destructions</q> de domaines avec la table de pointeurs du
vcpu allouée en utilisant l'hyperappel (1) XEN_DOMCTL_max_vcpus ou la table
de pointeurs du vcpu d'état xenoprofile allouée en utilisant l'hyperappel
(2) XENOPROF_get_buffer ou (3) XENOPROF_set_passive.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7970">CVE-2015-7970</a>

<p>La fonction p2m_pod_emergency_sweep de arch/x86/mm/p2m-pod.c dans
Xen 3.4.x, 3.5.x, et 3.6.x n'est pas préemptable. Cela permet à des
administrateurs de clients locaux x86 HVM de provoquer un déni de service
(consommation excessive de CPU et éventuellement redémarrage) grâce à des
contenus de mémoire contrefaits qui déclenchent un « time-consuming linear
scan » lié à « Populate-on-Demand ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7971">CVE-2015-7971</a>

<p>Xen, versions 3.2.x à 4.6.x, ne limite pas le nombre de messages de console
printk enregistrant certains pmu et profilant des hyperappels. Cela permet
à des clients locaux de provoquer un déni de service à l'aide d'une
séquence d'hyperappels contrefaits (1) HYPERCALL_xenoprof_op, qui ne sont
pas gérés correctement dans la fonction do_xenoprof_op dans
common/xenoprof.c, ou (2) HYPERVISOR_xenpmu_op qui ne sont pas gérés
correctement dans la fonction do_xenpmu_op dans arch/x86/cpu/vpmu.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7972">CVE-2015-7972</a>

<p>Les fonctions (1) libxl_set_memory_target dans tools/libxl/libxl.c et
(2) libxl__build_post dans tools/libxl/libxl_dom.c de Xen, versions 3.4.x
à 4.6.x, ne calculent pas correctement la taille du « ballon » lors de
l'utilisation du système « populate-on-demand » (PoD). Cela permet à des
utilisateurs client HVM locaux de provoquer un déni de service (plantage du
client) au moyen de vecteurs non précisés liés à « heavy memory pressure ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8104">CVE-2015-8104</a>

<p>Le sous-système KVM dans le noyau Linux jusqu'à 4.2.6, et Xen 4.3.x
jusqu'à 4.6.x, permet à des utilisateurs de système d'exploitation client
de provoquer un déni de service (panique du système d'exploitation hôte ou
blocage) en déclenchant de nombreuses exceptions #DB (ou Debug), liées
à svm.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8339">CVE-2015-8339</a>

<p>La fonction memory_exchange dans common/memory.c dans Xen,
versions 3.2.x à 4.6.x, ne gère pas correctement les pages de retour vers
un domaine, ce qui pourrait permettre à des administrateurs de système
d'exploitation client de provoquer un déni de service (plantage de l'hôte)
au moyen de vecteurs non précisés liés à la destruction de domaine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8340">CVE-2015-8340</a>

<p>La fonction memory_exchange dans common/memory.c dans Xen,
versions 3.2.x à 4.6.x ne libère pas correctement les verrous, ce qui
pourrait permettre à des administrateurs de système d'exploitation client
de provoquer un déni de service (blocage ou plantage de l'hôte) au moyen de
vecteurs non précisés liés au traitement d'erreur de XENMEM_exchange.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8550">CVE-2015-8550</a>

<p>Xen, quand il est utilisé sur un système fournissant des dorsaux de PV,
permet à des administrateurs locaux de système d'exploitation client de
provoquer un déni de service (plantage du système d'exploitation hôte) ou
d'obtenir des privilèges en écrivant sur la mémoire partagée entre
l'interface et le dorsal, autrement dit une vulnérabilité de type « double
fetch ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8554">CVE-2015-8554</a>

<p>Un dépassement de tampon dans hw/pt-msi.c dans Xen, version 4.6.x et
antérieures, lors de l'utilisation du modèle de périphérique
qemu-xen-traditional (ou qemu-dm), permet à des administrateurs locaux
client x86 HVM d'obtenir des privilèges en exploitant un système avec un
accès à un périphérique PCI physique intermédiaire compatible MSI-X et des
entrées de table MSI-X. Ce problème est lié à un « chemin d'écriture ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8555">CVE-2015-8555</a>

<p>Xen, versions 4.6.x, 4.5.x, 4.4.x, 4.3.x et antérieures, n'initialise
pas la pile FPU x86 et les registres XMM quand XSAVE ou XRSTOR ne sont pas
utilisés pour gérer l'état des registres étendus du client. Cela permet
à des domaines client locaux d'obtenir des informations sensibles à partir
d'autres domaines à l'aide de vecteurs non spécifiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8615">CVE-2015-8615</a>

<p>La fonction hvm_set_callback_via dans arch/x86/hvm/irq.c dans Xen 4.6
ne limite pas le nombre de messages de la console printk quand elle
enregistre la nouvelle méthode de rappel. Cela permet à des utilisateurs
locaux de systèmes d'exploitation HVM client de provoquer un déni de
service à l'aide d'un grand nombre de modifications de la méthode de rappel
(HVM_PARAM_CALLBACK_IRQ).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1570">CVE-2016-1570</a>

<p>La fonctionnalité superpage PV dans arch/x86/mm.c dans Xen,
version 3.4.0, 3.4.1 et 4.1.x à 4.6.x permet à des clients PV locaux
d'obtenir des informations sensibles, de provoquer un déni de service,
d'obtenir des privilèges ou d'avoir un autre impact non précisé à l'aide
d'un identifiant de page contrefait (MFN) pour (1) MMUEXT_MARK_SUPER ou
(2) MMUEXT_UNMARK_SUPER sub-op dans l'hyperappel HYPERVISOR_mmuext_op ou
(3) de vecteurs inconnus liés à des mises à jour de tables de pages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1571">CVE-2016-1571</a>

<p>La fonction paging_invlpg dans include/asm-x86/paging.h dans Xen
version 3.3.x à 4.6.x, quand l'utilisation de la pagination mode « shadow »
ou la virtualisation imbriquée sont activées, permet à des utilisateurs
locaux client HVM de provoquer un déni de service (plantage de l'hôte)
à l'aide d'une adresse client non canonique dans une instruction INVVPID, ce
qui déclenche une vérification de bogue de l'hyperviseur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2270">CVE-2016-2270</a>

<p>Xen, versions 4.6.x et antérieures, permet aux administrateurs de
clients locaux de provoquer un déni de service (redémarrage de l'hôte) par
des vecteurs liés à des mappages multiples de pages MMIO avec des
configurations différentes de mise en cache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2271">CVE-2016-2271</a>

<p>VMX dans Xen, versions 4.6.x et antérieure, lors de l'utilisation d'un
processeur Intel ou Cyrix, permet à des utilisateurs locaux client HVM de
provoquer un déni de service (plantage du client) grâce à des vecteurs liés
à un RIP non canonique.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-479.data"
# $Id: $
