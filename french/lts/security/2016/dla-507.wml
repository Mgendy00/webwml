#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans nss.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-4000">CVE-2015-4000</a>

<p>Avec les versions du protocole TLS 1.2 et antérieures, quand un ensemble
de chiffrement DHE_EXPORT est activé sur un serveur mais pas sur un client,
TLS ne transmet pas correctement un choix DHE_EXPORT. Cela permet à des
attaquants de type « homme du milieu » de conduire des attaques de
dégradation de chiffrement en réécrivant un « ClientHello » en remplaçant
DHE par DHE_EXPORT et ensuite en réécrivant un « ServerHello » avec
DHE_EXPORT remplacé par DHE, c'est-à-dire le problème appelé <q>Logjam</q>.</p>

<p>La solution dans nss était de ne pas accepter des largeurs inférieures
à 1024 bits. Cela peut éventuellement être un problème de
rétrocompatibilité mais ces faibles largeurs ne devraient pas être
utilisées, aussi la solution devrait être considérée comme acceptable.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2:3.14.5-1+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-507.data"
# $Id: $
