#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le paquet quagga installe des fichiers sensibles lisibles par tous dans
/etc/quagga et pourrait être sujet à un déni de service du fait d'une
absence de vérification de taille de paquet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4036">CVE-2016-4036</a>

<p>Le paquet quagga avant la version 0.99.23-2.6.1 utilise des permissions
faibles pour l'accès à /etc/quagga. Cela permet à des utilisateurs locaux
d'obtenir des informations sensibles en lisant des fichiers dans ce
répertoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4049">CVE-2016-4049</a>

<p>La fonction bgp_dump_routes_func dans bgpd/bgp_dump.c dans Quagga ne
réalise pas de vérification de taille lorsqu'elle dépose des données. Cela
pourrait permettre à des attaquants distants de provoquer un déni de
service (échec d'assertion et plantage du démon) à l'aide d'un grand paquet
BGP.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.99.22.4-1+wheezy3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets quagga.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-601.data"
# $Id: $
