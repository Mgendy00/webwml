#use wml::debian::translation-check translation="7399be288c66a990e13ba163e87f97a70e9334bf" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans SPIP, un moteur de
publication pour site web écrit en PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7980">CVE-2016-7980</a>

<p>Nicolas Chatelain de Sysdream Labs a découvert une vulnérabilité de
contrefaçon de requête intersite (CSRF) dans l'action valider_xml de SPIP.
Cela permet à des attaquants distants de faire usage de vulnérabilités
éventuelles supplémentaires telles que celle décrite dans le
<a href="https://security-tracker.debian.org/tracker/CVE-2016-7998">CVE-2016-7998</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7981">CVE-2016-7981</a>

<p>Nicolas Chatelain de Sysdream Labs a découvert une vulnérabilité
d'attaque par script intersite réfléchie (XSS) dans l'action validater_xml
de SPIP. Un attaquant pourrait tirer avantage de cette vulnérabilité pour
injecter du code arbitraire en piégeant un administrateur dans l'ouverture
d'un lien malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7982">CVE-2016-7982</a>

<p>Nicolas Chatelain de Sysdream Labs a découvert une attaque d'énumération
de fichiers ou de traversée de répertoires dans l'action validator_xml de
SPIP. Un attaquant pourrait utiliser cela pour énumérer les fichiers dans
un répertoire arbitraire dans le système de fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7998">CVE-2016-7998</a>

<p>Nicolas Chatelain de Sysdream Labs a découvert une possible
vulnérabilité d'exécution de code PHP dans les fonctions <q>template
compiler</q> ou <q>template composer</q> de SPIP. En combinaison avec les
vulnérabilités XSS et CSRF décrites dans cette annonce, un attaquant
distant pourrait tirer avantage de cela pour exécuter du code PHP
arbitraire sur le serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7999">CVE-2016-7999</a>

<p>Nicolas Chatelain de Sysdream Labs a découvert une contrefaçon de
requête côté serveur dans l'action valider_xml de SPIP. Des attaquants
pourraient tirer avantage de cette vulnérabilité pour envoyer des requêtes
HTTP ou FTP à des serveurs distants auxquels ils n'ont pas d'accès direct,
éventuellement en contournant des contrôles d'accès tels qu'un pare-feu.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.1.17-1+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spip.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-695.data"
# $Id: $
