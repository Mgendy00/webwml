#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La bibliothèque exiv2 est vulnérable à plusieurs problèmes qui peuvent tous
aboutir à un déni de service de l’application s’appuyant sur la bibliothèque
pour analyser les métadonnées des images.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11591">CVE-2017-11591</a>

<p>Déni de service à l’aide d’exception de virgule flottante dans la fonction
Exiv2::ValueType.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11683">CVE-2017-11683</a>

<p>Déni de service à travers l’échec d’assertion déclenché par une image contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14859">CVE-2017-14859</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-14862">CVE-2017-14862</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-14864">CVE-2017-14864</a>

<p>Déni de service à travers un accès mémoire non valable déclenché par une
image contrefaite.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.23-1+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exiv2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1147.data"
# $Id: $
