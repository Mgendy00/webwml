#use wml::debian::translation-check translation="b79f46e2a74b9e4c9da135bed1121f70cdf26267" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été récemment découvertes dans TightVNC 1.x, une
application serveur/visualisateur VNC basée sur X11 pour Windows et Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6053">CVE-2014-6053</a>

<p>La fonction rfbProcessClientNormalMessage dans rfbserver.c dans le serveur
TightVNC ne gérait pas correctement les essais d’envoi de grandes quantités de
données ClientCutText. Cela permettait à des attaquants distants de provoquer un
déni de service (consommation de mémoire ou plantage du démon) à l'aide d'un
message contrefait qui était traité en utilisant une seule malloc non vérifiée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7225">CVE-2018-7225</a>

<p>rfbProcessClientNormalMessage() dans rfbserver.c ne nettoyait pas
msg.cct.length, conduisant à un accès à des données non initialisées et
éventuellement sensibles ou éventuellement à un autre impact non précisé (par
exemple, un dépassement d'entier) à l’aide de paquets VNC contrefaits pour
l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8287">CVE-2019-8287</a>

<p>Le code de TightVNC contenait un dépassement global de tampon dans
la fonction de macro HandleCoRREBBP. Cela pouvait éventuellement aboutir à une
exécution de code. Cette attaque semble être exploitable à l’aide d’une
connectivité réseau.</p>

<p>(c'est-à-dire, <a href="https://security-tracker.debian.org/tracker/CVE-2018-20020">CVE-2018-20020</a>/libvncserver)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20021">CVE-2018-20021</a>

<p>TightVNC dans vncviewer/rfbproto.c contenait une vulnérabilité de
boucle infinie (CWE-835). La vulnérabilité permettait à un attaquant de consommer un
montant excessif de ressources telles que celles du CPU et de la RAM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20022">CVE-2018-20022</a>

<p>vncviewer de TightVNC contenait plusieurs faiblesses : une
vulnérabilité d’initialisation incorrecte dans le code du client VNC (CWE-665) permettait
à des attaquants de lire la mémoire de pile et pourrait être traitée
incorrectement pour une divulgation d'informations. Combinée avec une autre
vulnérabilité, elle pouvait être utilisée pour divulguer la disposition de la
mémoire de pile et pour contourner l’ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15678">CVE-2019-15678</a>

<p>La version de code TightVNC contenait un dépassement de tampon basé sur le
tas dans le gestionnaire rfbServerCutText. Cela pouvait aboutir éventuellement
à une exécution de code. Cette attaque semble être exploitable à l’aide d’une
connectivité réseau.</p>

<p>(c'est-à-dire, partiellement <a href="https://security-tracker.debian.org/tracker/CVE-2018-20748">CVE-2018-20748</a>/libvnvserver)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15679">CVE-2019-15679</a>

<p>Le code de vncviewer de TightVNC contenait un dépassement de tampon basé sur
le tas dans la fonction InitialiseRFBConnection. Cela pouvait aboutir
éventuellement à une exécution de code. Cette attaque semble être exploitable
à l’aide d’une connectivité réseau.</p>

<p>(c'est-à-dire, partiellement <a href="https://security-tracker.debian.org/tracker/CVE-2018-20748">CVE-2018-20748</a>/libvnvserver)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15680">CVE-2019-15680</a>

<p>Le code de vncviewer de TightVNC contenait un déréférencement de pointeur
NULL dans la fonction HandleZlibBPP. Cela pouvait aboutir à un déni de service.
Cette attaque semble être exploitable à l’aide d’une connectivité réseau.</p>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15681">CVE-2019-15681</a>

<p>TightVNC contenait une fuite de mémoire (CWE-655) dans le code du serveur de
VNC. Cela permettait à un attaquant de lire la mémoire de pile et pouvait être
traitée incorrectement pour une divulgation d'informations. Combinée avec une
autre vulnérabilité, elle pouvait être utilisée pour divulguer de la mémoire de
pile et pour contourner l’ASLR. Cette attaque semble être exploitable à l’aide
d’une connectivité réseau.</p>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.9-6.5+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tightvnc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2045.data"
# $Id: $
