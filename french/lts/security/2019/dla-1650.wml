#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>L’équipe de sécurité d’ESnet a découvert une vulnérabilité dans rssh, un
interpréteur restreint, qui permet aux utilisateurs de réaliser uniquement des
opérations scp, sftp, cvs, svnserve (Subversion), rdist ou rsync. Une absence de
vérification dans la prise en charge de scp pourrait mener au contournement de
cette restriction, permettant l’exécution de commandes d’interpréteur
arbitraires.</p>

<p>Veuillez noter qu’avec l’application de cette mise à jour, l’option «·-3·» de
scp ne peut plus être utilisée.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 2.3.4-4+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rssh.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1650.data"
# $Id: $
