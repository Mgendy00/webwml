#use wml::debian::translation-check translation="e41f5efa353b2bdd34609a011c02c9132873e041" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans l'implémentation du
protocole WPA présent dans wpa_supplication (poste) et hostapd (point
d'accès).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13377">CVE-2019-13377</a>

<p>Une attaque temporelle par canal auxiliaire à l'encontre du « Dragonfly
handshake » de WPA3 lors de l'utilisation de courbes Brainpool pourrait
être utilisée par un attaquant pour récupérer le mot de passe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16275">CVE-2019-16275</a>

<p>Une validation insuffisante d'adresse de source pour certaines trames de
gestion reçues dans hostapd pourrait conduire à un déni de service pour des
postes associés à un point d'accès. Un attaquant dans la zone de réception
radio du point d'accès pourrait injecter au point d'accès une trame
IEEE 802.11 non authentifiée spécialement construite afin de provoquer la
déconnexion des postes associés et requérir une reconnexion au réseau.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2:2.7+git20190128+0c1e29f-6+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4538.data"
# $Id: $
