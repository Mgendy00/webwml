#use wml::debian::template title="As pessoas: quem somos e o que fazemos"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ac8343f61b83fe988dbcb035d77af5bf09ba0709"

# tradutores(as): parte do texto foi retirado de /intro/about.wml

<h2>Desenvolvedores(as) e contribuidores(as)</h2>
<p>O Debian é produzido por aproximadamente mil desenvolvedores(as)
ativos(as) espalhados(as)
<a href="$(DEVEL)/developers.loc">pelo mundo</a> que se voluntariam em seu
tempo livre.
Poucos(as) desenvolvedores(as) se conhecem pessoalmente. A comunicação é
realizada principalmente através de e-mail (listas de discussão
em lists.debian.org) e IRC (canal #debian-br em português, ou #debian em inglês,
no irc.debian.org).
</p>

<p>A lista completa de participantes oficiais do Debian pode ser encontrada em
<a href="https://nm.debian.org/members">nm.debian.org</a>, onde a comunidade é
gerenciada. Uma lista mais ampla de contribuidores(as) do Debian pode ser
encontrada em
<a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<p>O Projeto Debian tem uma <a href="organization">estrutura organizada</a>
cuidadosamente. Para mais informações sobre como o Debian é internamente,
sinta-se livre para navegar pelo
<a href="$(DEVEL)/">canto dos(as) desenvolvedores(as)</a>.</p>

<h3><a name="history">Como tudo isso começou?</a></h3>

<p>O Debian foi criado em agosto de 1993 por Ian Murdock, como uma nova
distribuição que seria feita abertamente, no espírito do Linux e do projeto GNU.
O Debian foi concebido para ser cuidadosamente e conscientemente organizado, e
para ser mantido e suportado com cuidado similar. Tudo começou como um pequeno
grupo de hackers de Software Livre que, gradativamente, cresceu e tornou-se uma
grande e organizada comunidade de desenvolvedores(as) e usuários(as). Veja
<a href="$(DOC)/manuals/project-history/">a história detalhada</a>.

<p>Já que muitas pessoas perguntaram, Debian é pronunciado /&#712;de.bi.&#601;n/.
O nome vem do nome de seu criador, Ian Murdock, e sua esposa, Debra.

<h2>Pessoas e organizações que suportam o Debian</h2>

<p>Muitos outros indivíduos e organizações fazem parte da comunidade Debian:
<ul>
  <li><a href="https://db.debian.org/machines.cgi">Patrocinadores(as) de hospedagem e hardware</a></li>
  <li><a href="../mirror/sponsors">Patrocinadores(as) de espelhos</a></li>
  <li><a href="../partners/">Parceiros(as) de desenvolvimento e serviço</a></li>
  <li><a href="../consultants">Consultores(as)</a></li>
  <li><a href="../CD/vendors">Vendedores(as) de mídias de instalação do Debian</a></li>
  <li><a href="../distrib/pre-installed">Vendedores(as) de computadores que pré-instalam o Debian</a></li>
  <li><a href="../events/merchandise">Vendedores(as) de merchandising</a></li>
</ul>

<h2><a name="users">Quem usa o Debian?</a></h2>

<p>Embora não exista uma estatística precisa (já que o Debian não exige que
usuários(as) se registrem), existem fortes evidências de que o Debian é
utilizado por um amplo número de grandes e pequenas organizações, bem como por
milhares de pessoas. Veja nossa página
<a href="../users/">quem está usando o Debian?</a> para uma lista de
organizações importantes que nos enviaram uma breve descrição de como e porque
estão usando o Debian.
