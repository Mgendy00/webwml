#use wml::debian::template title="Εγκαταστάτης του Debian" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="6f892e2bb4879338aa7540b6e970f730a9674781" maintainer="galaxico"

<h1>Ειδήσεις</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Παλιότερες ειδήσεις</a>
</p>

 
<h1>Εγκαθιστώντας με τον Εγκαταστάτη του Debian</h1>

<p>
<if-stable-release release="bullseye">
<strong>Για επίσημα μέσα εγκατάστασης του Debian <current_release_bullseye> και 
πληροφορίες</strong>, δείτε τη
<a href="$(HOME)/releases/bullseye/debian-installer">σελίδα του bullseye</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>Για επίσημα μέσα εγκατάστασης του Debian <current_release_bookworm>  
και πληροφορίες</strong>, δείτε την σελίδα της έκδοσης
<a href="$(HOME)/releases/bookworm/debian-installer">bookworm</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Όλοι οι σύνδεσμοι σε εικόνες αρχείων που ακολουθούν είναι για την έκδοση του 
Εγκαταστάτη του Debian που είναι υπό ανάπτυξη για την επόμενη έκδοση του Debian 
και θα εγκαταστήσουν εξ ορισμού τη δοκιμαστική έκδοση (<q><current_testing_name></q>).
</p>
</div>


<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">
<p>

<strong>Για να εγκαταστήσετε την δοκιμαστική έκδοση του Debian</strong>, συνιστούμε την χρήση
των <strong>καθημερινών μεταγλωττίσεων (builds)</strong> του εγκαταστάτη. Για αυτές είναι διαθέσιμες
οι ακόλουθες εικόνες:

</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">
<p>

<strong>Για να εγκαταστήσετε την δοκιμαστική έκδοση του Debian</strong>, συνιστούμε τη χρήση της 
<strong><humanversion /></strong> έκδοσης του εγκαταστάτη αφού ελέγξετε πρώτα τα
<a href="errata">παροράματά της</a>. Είναι διαθέσιμες οι ακόλουθες εικόνες για την
<humanversion />:

</p>

<h2>Επόσημη έκδοση</h2>

<div class="line">
<div class="item col50">
<strong>εικόνες netinst CD (γενικά 180-450 MB)</strong>
<netinst-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray  (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>άλλες εικόνες (netboot, κλειδιά USB, κλπ.)</strong>
<other-images />
</div>
</div>


<p>
Ή εγκαταστήστε το <b>τρέχον εβδομαδιαίο στιγμιότυπο της δοκιμαστικής έκδοσης του Debian</b>
που χρησιμοποιεί την ίδια έκδοση του εγκαταστάτη με αυτήν της τελευταίας σταθερής έκδοσης:
</p>

<h2>Τρέχοντα εβδομαδιαία στιγμιότυπα</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

<p>
Αν προτιμάτε να χρησιμοποιήσετε ό,τι πιο πρόσφατο, είτε για να μας βοηθήσετε
να δοκιμάσουμε μια μελλοντική έκδοση του εγκαταστάτη είτε εξαιτίας
προβλημάτων με το υλικό σας ή άλλων προβλημάτων, δοκιμάστε μια από αυτές 
τις <strong>καθημερινά μεταγλωττισμένες εικόνες</strong> που περιέχουν την πιο
πρόσφατη διαθέσιμη έκδοση των συστατικών του εγκαταστάτη.
</p>
</if-testing-installer>

<h2>Τρέχοντα καθημερινά στιγμιότυπα</h2>

<div class="line">
<div class="item col50">
<strong>εικόνες netinst CD (γενικά 150-280 MB) <!-- and businesscard (generally 20-50 MB) --> </strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>εικόνες netinst CD <!-- and businesscard -->  (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>εικόνες netinst CD πολλαπλών αρχιτεκτονικών</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>άλλες εικόνες (netboot, κλειδιά USB, κλπ.)</strong>
<devel-other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>

Αν οποιοδήποτε υλικό στο σύστημά σας <strong>απαιτεί τη φόρτωση μη-ελεύθερου υλισμικού 
(firmware)</strong> μέσω ενός οδηγού συσκευής, μπορείτε να χρησιμοποιήσετε ένα 
από τα <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/">
συμπιεσμένα αρχεία πακέτων κοινού υλισμικού</a> ή να μεταφορτώσετε μια <strong>ανεπίσημη</strong> εικόνα 
που περιλαμβάνει αυτά τα είδη <strong>μη-ελεύθερου</strong> υλισμικού. Οδηγίες για την εγκατάσταση 
των συμπιεσμένων αρχείων και γενικότερη τεκμηρίωση για τη φόρτωση υλισμικού στη διάρκεια 
μιας εγκατάστασης μπορεί να βρεθεί στον <a href="https://d-i.debian.org/doc/installation-guide/en.amd64/ch06s04.html">Οδηγό Εγκατάστασης</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/daily-builds/sid_d-i/current/">ανεπίσημες εικόνες 
που συμπεριλαμβάνουν υλισμικό - καθημερινές εκδόσεις</a>
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-builds/">ανεπίσημες εικόνες 
που συμπεριλαμβάνουν υλισμικό - εβδομαδιαίες εκδόσεις</a>

</p>
</div>

<hr />


<p>
<strong>Σημειώσεις</strong>
</p>
<ul>
#	<li>Before you download the daily built images, we suggest you check for
#	<a href="https://wiki.debian.org/DebianInstaller/Today">known issues</a>.</li>
	<li>Μια αρχιτεκτονική μπορεί (προσωρινά) να παραλειφθεί από την επισκόπηση 
των ημερήσια δημιουργούμενων εικόνων αν αυτές δεν είναι (αξιόπιστα) 
διαθέσιμες.</li>
	<li>Για τις εικόνες αρχείων εγκατάστασης διατίθενται αρχεία επαλήθευσης 
(<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt> και άλλα) στο ίδιον κατάλογο στον 
οποίο βρίσκονται οι αντίστοιχες εικόνες.</li>
	<li>Για τη μεταφόρτωση εικόνων ολόκληρων CD και DVD συνιστάται η χρήση του 
jigdo.</li>
	<li>Μόνο ένας περιορισμένος αριθμός εικόνων από τα σετ CD και DVD sets 
είναι διαθέσιμος ως αρχεία ISO για απευθείας μεταφόρτωση. Οι περισσότεροι 
χρήστες δεν χρειάζονται όλο το λογισμικό που βρίσκεται στο σύνολο των δίσκων, 
οπότε για να εξοικονομηθεί χώρος στους εξυπηρετητές μεταφόρτωσης και στους 
"καθρέφτες" των αρχειοθηκών του Debian, τα πλήρη σετ είναι διαθέσιμα μόνο 
μέσω jigdo.</li>
	<li>Η εικόνα <em>CD</em> πολλαπλών αρχιτεκτονικών υποστηρίζει τις 
αρχιτεκτονικές i386/amd64· η εγκατάσταση είναι παρόμοια με την εγκατάσταση από 
μια εικόνα netinst μιας μοναδικής αρχιτεκτονικής.</li>
</ul>

<p>
<strong>Μετά τη χρήση του Εγκαταστάτη του Debian</strong>, παρακαλούμε 
στείλτε μας μια 
<a
href="https://d-i.debian.org/manual/el.amd64/ch05s04.html#submit-bug">
αναφορά εγκατάστασης</a>,
ακόμα και στην περίπτωση που δεν υπήρξαν οποιαδήποτε προβλήματα.
</p>

<h1>Τεκμηρίωση</h1>

<p>
<strong>Αν πρόκειται να διαβάσετε ένα μοναδικό κείμενο </strong> πριν την 
εγκατάσταση, τότε διαβάστε το
<a href="https://d-i.debian.org/manual/el.amd64/apa.html">Howto της 
εγκατάστασης</a>, μια σύντομη περιήγηση στη διαδικασία της εγκατάστασης. 
Άλλα χρήσιμα είδη τεκμηρίωσης περιλαμβάνουν:
</p>

<ul>
<li>Οδηγός Εγκατάστασης:
#    <a href="$(HOME)/releases/stable/installmanual">έκδοση της τρέχουσας 
#έκδοσης</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">υπό ανάπτυξη 
έκδοση (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">πιο πρόσφατη έκδοση (Git)</a>
<br />
λεπτομερείς οδηγίες εγκατάστασης</li>
<li><a 
href="https://wiki.debian.org/DebianInstaller/FAQ">Συχνές ερωτήσεις του 
Εγκαταστάτη του Debian</a>
and <a href="$(HOME)/CD/faq/">Συχνές ερωτήσεις των CD του Debian</a><br 
/>
κοινές ερωτήσεις και απαντήσεις</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki του 
Εγκαταστάτη του Debian</a><br />
τεκμηρίωση συντηρούμενη από την κοινότητα</li>
</ul>

<h1>Επικοινωνήστε μαζί μας</h1>

<p>
Η  <a href="https://lists.debian.org/debian-boot/">λίστα 
αλληλογραφίας debian-boot</a> είναι το κύριο φόρμουμ για συζήτηση και δουλειά 
σχετικά με τον Εγκαταστάτη του Debian.
</p>

<p>
Διαθέτουμε επίσης ένα κανάλι IRC, #debian-boot στον ιστότοπο 
<tt>irc.debian.org</tt>. Το κανάλι αυτό χρησιμοποιείται κυρίως για ανάπτυξη 
αλλά περιστασιακά και για υποστήριξη. Αν δεν πάρετε κάποια απάντηση, 
χρησιμοποιήστε εναλλακτικά τη λίστα.
</p>

