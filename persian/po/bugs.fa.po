msgid ""
msgstr ""
"Project-Id-Version: Debian webwml bugs\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2015-05-17 03:12+0330\n"
"Last-Translator: Behrad Eslamifar <behrad_es@yahoo.com>\n"
"Language-Team: debian-l10n-persian <debian-l10n-persian@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Abbas Esmaeeli Some'eh <abbas@gnu.NOSPAM.org>\n"
"X-Poedit-Language: Persian\n"
"X-Poedit-Country: IRAN, ISLAMIC REPUBLIC OF\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "در بسته"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "برچسب زده شده"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "با درجه شدت"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "در بسته منبع"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "بسته ها مدیریت می شوند توسط"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "ارسال شده توسط"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "به مالکیت"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "با وضعیت"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "جدیدترین باگ ها"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "با بسته"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "عادی"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "ظاهر قدیمی"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "سطر"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "عمر"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "تکرار ادقام"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "سختی معکوس:"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "هیچکدام"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "آزمایشی"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "پایدار قدیمی"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "پایدار"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "تجربی"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "ناپایدار"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "بایگانی نشده"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "بایگانی شده"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "بایگانی شده و بایگانی نشده"

#~ msgid "Flags:"
#~ msgstr "پرچم‌ها"

#~ msgid "active bugs"
#~ msgstr "اشکالات حل نشده"

#~ msgid "display merged bugs only once"
#~ msgstr "اشکالات ادغام شده را فقط یک بار نمایش بده"

#~ msgid "no ordering by status or severity"
#~ msgstr "ترتیب بدون توجه به وضعیت یا درجه شدت"

#~ msgid "don't show table of contents in the header"
#~ msgstr "عدم نمایش فهرست محتویات در سرایند"

#~ msgid "don't show statistics in the footer"
#~ msgstr "عدم نمایش آمارها در زیرنویس‌ها"

#~ msgid "proposed-updates"
#~ msgstr "به روز رسانی‌های پیشنهادی"

#~ msgid "testing-proposed-updates"
#~ msgstr "به روز رسانی‌های پیشنهادی آزمایشی"

#~ msgid "Package version:"
#~ msgstr "نسخه بسته:"

#~ msgid "Distribution:"
#~ msgstr "توزیع:"

#~ msgid "bugs"
#~ msgstr "اشکالات"

#~ msgid "open"
#~ msgstr "باز"

#~ msgid "forwarded"
#~ msgstr "فرستاده شده"

#~ msgid "pending"
#~ msgstr "در جریان"

#~ msgid "fixed"
#~ msgstr "حل شده"

#~ msgid "done"
#~ msgstr "انجام شده"

#~ msgid "Include status:"
#~ msgstr "شامل وضعیت:"

#~ msgid "Exclude status:"
#~ msgstr "کنارگذاری وضعیت:"

#~ msgid "critical"
#~ msgstr "بحرانی"

#~ msgid "grave"
#~ msgstr "خطرناک"

#~ msgid "serious"
#~ msgstr "جدی"

#~ msgid "important"
#~ msgstr "مهم"

#~ msgid "minor"
#~ msgstr "جزئی"

#~ msgid "wishlist"
#~ msgstr "فهرست خواسته‌ها"

#~ msgid "Include severity:"
#~ msgstr "شامل درجه شدت:"

#~ msgid "Exclude severity:"
#~ msgstr "کنارگذاری درجه شدت:"

#~ msgid "potato"
#~ msgstr "پوتاتو"

#~ msgid "woody"
#~ msgstr "وودی"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-ignore"

#~ msgid "etch"
#~ msgstr "اچ"

#~ msgid "etch-ignore"
#~ msgstr "etch-ignore"

#~ msgid "lenny"
#~ msgstr "lenny"

#~ msgid "lenny-ignore"
#~ msgstr "lenny-ignore"

#~ msgid "sid"
#~ msgstr "سید"

#~ msgid "confirmed"
#~ msgstr "تايید شده"

#~ msgid "d-i"
#~ msgstr "d-i"

#~ msgid "fixed-in-experimental"
#~ msgstr "حل شده در تجربی"

#~ msgid "fixed-upstream"
#~ msgstr "fixed-upstream"

#~ msgid "help"
#~ msgstr "کمک"

#~ msgid "l10n"
#~ msgstr "محلی‌سازی"

#~ msgid "moreinfo"
#~ msgstr "اطلاعات بیشتر"

#~ msgid "patch"
#~ msgstr "وصله"

#~ msgid "security"
#~ msgstr "امنیت"

#~ msgid "unreproducible"
#~ msgstr "غیر قابل تولید مجدد"

#~ msgid "upstream"
#~ msgstr "upstream"

#~ msgid "wontfix"
#~ msgstr "حل نخواهد شد"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "Include tag:"
#~ msgstr "شامل برچسب:"

#~ msgid "Exclude tag:"
#~ msgstr "کنارگذاری برچسب:"
