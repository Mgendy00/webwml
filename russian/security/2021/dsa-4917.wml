#use wml::debian::translation-check translation="1fe97ede528cadc78447b1edeb9a753b00b0639c" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В веб-браузере chromium было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30506">CVE-2021-30506</a>

    <p>@retsew0x01 обнаружил ошибку в установочном интерфейсе Web App.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30507">CVE-2021-30507</a>

    <p>Элисон Хаффмэн обнаружила ошибку в режиме автономной работы.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30508">CVE-2021-30508</a>

    <p>Leecraso и Гуан Гон обнаружили переполнение буфера в реализации
    Media Feeds.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30509">CVE-2021-30509</a>

    <p>Давид Эрцег обнаружил запись за пределами выделенного буфера памяти в реализации
    Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30510">CVE-2021-30510</a>

    <p>Вэйпэн Цзян обнаружил состояние гонки в оконном менеджере aura.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30511">CVE-2021-30511</a>

    <p>Давид Эрцег обнаружил чтение за пределами выделенного буфера памяти в реализации
    Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30512">CVE-2021-30512</a>

    <p>ЧжаньЦзя Сун обнаружил использование указателей после освобождения памяти в реализации
    уведомлений.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30513">CVE-2021-30513</a>

    <p>Мань Юэ Мо обнаружил использование неправильного типа в javascript-библиотеке v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30514">CVE-2021-30514</a>

    <p>koocola и Ван обнаружили использование указателей после освобождения памяти в
    функции автозаполнения форм.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30515">CVE-2021-30515</a>

    <p>Жун Цзянь и Гуан Гун обнаружили использование указателей после освобождения памяти в системном
    API доступа к файлам.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30516">CVE-2021-30516</a>

    <p>ЧжаньЦзя Сун обнаружил переполнение буфера в истории просмотра.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30517">CVE-2021-30517</a>

    <p>Цзюнь Кокатсу обнаружил переполнение буфера в режиме чтения.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30518">CVE-2021-30518</a>

    <p>laural обнаружил использование неправильного типа в javascript-библиотеке v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30519">CVE-2021-30519</a>

    <p>asnine обнаружил использование указателей после освобождения памяти в платежах.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30520">CVE-2021-30520</a>

    <p>Халил Жани обнаружил использование указателей после освобождения памяти в реализации
    Tab Strip.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 90.0.4430.212-1~deb10u1.</p>

<p>Рекомендуется обновить пакеты chromium.</p>

<p>С подробным статусом поддержки безопасности chromium можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4917.data"
