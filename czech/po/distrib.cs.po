# distrib.cs.po
# Copyright (C) 2004 Free Software Foundation, Inc.
# Juraj Kubelka <Juraj.Kubelka@email.cz>, 2004
# Michal Simunek <michal.simunek@gmail.com>, 2009 - 2011
#
msgid ""
msgstr ""
"Project-Id-Version: distrib 1.10\n"
"PO-Revision-Date: 2015-04-18 14:07+0200\n"
"Last-Translator: Michal Simunek <michal.simunek@gmail.com>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Klíčové slovo"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Zobrazit"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "cesty končící klíčovým slovem"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "balíčky s takto nazvanými soubory"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "balíčky se soubory jejichž názvy obsahuje klíčové slovo"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Distribuce"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "experimental"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "unstable"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "testing"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "stable"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "oldstable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Architektura"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "jakákoliv"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Hledat"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Vyprázdnit"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Vyhledat"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Pouze v názvech balíčků"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "V popisech"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "V názvech zdrojových balíčků"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Zobrazit pouze při přesné shodě"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Sekce"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64 bitové PC (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
#, fuzzy
msgid "64-bit ARM (AArch64)"
msgstr "64 bitové PC (amd64)"

#: ../../english/releases/arches.data:12
#, fuzzy
msgid "EABI ARM (armel)"
msgstr "ARM EABI"

#: ../../english/releases/arches.data:13
#, fuzzy
msgid "Hard Float ABI ARM (armhf)"
msgstr "ARM s hardwarovým FPU"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd pro 32 bitové PC (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "32 bitové PC (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD pro 32 bitové PC (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD pro 64 bitové PC (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (big endian)"

#: ../../english/releases/arches.data:22
#, fuzzy
msgid "64-bit MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr ""

#: ../../english/releases/arches.data:26
#, fuzzy
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr "SPARC"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"

#~ msgid "AMD64"
#~ msgstr "AMD64"
