<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that the original fix for <a href="https://security-tracker.debian.org/tracker/CVE-2015-7552">CVE-2015-7552</a> (DLA-450-1)
was incomplete.</p>

<p>A heap-based buffer overflow in gdk-pixbuf, a library for image
loading and saving facilities, fast scaling and compositing of
pixbufs, allows remote attackers to cause a denial of service or
possibly execute arbitrary code via a crafted BMP file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.26.1-1+deb7u5.</p>

<p>We recommend that you upgrade your gdk-pixbuf packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-501.data"
# $Id: $
