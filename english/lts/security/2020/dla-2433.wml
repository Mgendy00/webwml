<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the
bouncycastle crypto library where attackers could
obtain sensitive information due to observable
differences in its response to invalid input.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26939">CVE-2020-26939</a>

    <p>In Legion of the Bouncy Castle BC before 1.55 and
    BC-FJA before 1.0.1.2, attackers can obtain sensitive
    information about a private exponent because of
    Observable Differences in Behavior to Error Inputs. This
    occurs in
    org.bouncycastle.crypto.encodings.OAEPEncoding. Sending
    invalid ciphertext that decrypts to a short payload in
    the OAEP Decoder could result in the throwing of an
    early exception, potentially leaking some information
    about the private exponent of the RSA private key
    performing the encryption.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.56-1+deb9u3.</p>

<p>We recommend that you upgrade your bouncycastle packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2433.data"
# $Id: $
