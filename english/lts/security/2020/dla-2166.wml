<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The krb5 PAM module (pam_krb5.so) had a buffer overflow that might have
caused remote code execution in situations involving supplemental
prompting by a Kerberos library. It might have overflown a buffer
provided by the underlying Kerberos library by a single '\0' byte if an
attacker responded to a prompt with an answer of a carefully chosen
length. The effect may have ranged from heap corruption to stack
corruption depending on the structure of the underlying Kerberos library,
with unknown effects but possibly including code execution. This code
path had not been used for normal authentication, but only when the
Kerberos library did supplemental prompting, such as with PKINIT or when
using the non-standard no_prompt PAM configuration option.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.6-3+deb8u1.</p>

<p>We recommend that you upgrade your libpam-krb5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2166.data"
# $Id: $
