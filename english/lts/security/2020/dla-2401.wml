<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Sympa, a modern mailing list manager, allows privilege escalation
through setuid wrappers. A local attacker can obtain root access.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
6.2.16~dfsg-3+deb9u3.</p>

<p>We recommend that you upgrade your sympa packages.</p>

<p>For the detailed security status of sympa please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sympa">https://security-tracker.debian.org/tracker/sympa</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2401.data"
# $Id: $
