<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that ClamAV, an antivirus software, was susceptible to a
denial of service attack by unauthenticated users via inefficient MIME
parsing of especially crafted email files.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.101.5+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2108.data"
# $Id: $
