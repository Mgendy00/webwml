<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Carlo Arenas discovered a flaw in git, a fast, scalable, distributed
revision control system. With a crafted URL that contains a newline or
empty host, or lacks a scheme, the credential helper machinery can be
fooled into providing credential information that is not appropriate for
the protocol in use and host being contacted.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2.1.4-2.1+deb8u10.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2182.data"
# $Id: $
