<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The server component of Apache Guacamole, a remote desktop gateway,
did not properly validate data received from RDP servers. This could
result
in information disclosure or even the execution of arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9497">CVE-2020-9497</a>

    <p>Apache Guacamole does not properly validate data received from RDP
    servers via static virtual channels. If a user connects to a
    malicious or compromised RDP server, specially-crafted PDUs could
    result in disclosure of information within the memory of the guacd
    process handling the connection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9498">CVE-2020-9498</a>

    <p>Apache Guacamole may mishandle pointers involved in processing data
    received via RDP static virtual channels. If a user connects to a
    malicious or compromised RDP server, a series of specially-crafted
    PDUs could result in memory corruption, possibly allowing arbitrary
    code to be executed with the privileges of the running guacd
    process.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.9.9-2+deb9u1.</p>

<p>We recommend that you upgrade your guacamole-server packages.</p>

<p>For the detailed security status of guacamole-server please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/guacamole-server">https://security-tracker.debian.org/tracker/guacamole-server</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2435.data"
# $Id: $
