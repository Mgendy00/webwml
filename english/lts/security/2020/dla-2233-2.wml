<define-tag description>LTS regression update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a regression in the latest update to
Django, the Python web development framework. The upstream fix for
CVE-2020-13254 to address data leakages via malformed memcached keys
could, in some situations, cause a traceback.</p>

<p>Please see <tt>https://code.djangoproject.com/ticket/31654</tt> for more
information.</p>

<ul>
<li>
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-13254">
        CVE-2020-13254: Potential data leakage via malformed memcached keys.
    </a>

    <p>In cases where a memcached backend does not perform key validation,
    passing malformed cache keys could result in a key collision, and potential
    data leakage. In order to avoid this vulnerability, key validation is added
    to the memcached cache backends.</p>
</li>
</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.11-1+deb8u10.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2233-2.data"
# $Id: $
