<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Security researchers from Snyk discovered that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-9658">CVE-2019-9658</a>
was incomplete. Checkstyle, a development tool to help programmers write
Java code that adheres to a coding standard, was still vulnerable to XML
External Entity (XXE) injection.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.9-1+deb8u2.</p>

<p>We recommend that you upgrade your checkstyle packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2099.data"
# $Id: $
