<define-tag description>LTS new package</define-tag>
<define-tag moreinfo>
<p>Linux 4.19 has been packaged for Debian 9 as linux-4.19.  This
provides a supported upgrade path for systems that currently use
kernel packages from the "stretch-backports" suite.</p>

<p>However, "apt full-upgrade" will *not* automatically install the
updated kernel packages.  You should explicitly install one of the
following metapackages first, as appropriate for your system:</p>

<ul>
  <li>linux-image-4.19-686</li>
  <li>linux-image-4.19-686-pae</li>
  <li>linux-image-4.19-amd64</li>
  <li>linux-image-4.19-arm64</li>
  <li>linux-image-4.19-armmp</li>
  <li>linux-image-4.19-armmp-lpae</li>
  <li>linux-image-4.19-cloud-amd64</li>
  <li>linux-image-4.19-marvell</li>
  <li>linux-image-4.19-rpi</li>
  <li>linux-image-4.19-rt-686-pae</li>
  <li>linux-image-4.19-rt-amd64</li>
  <li>linux-image-4.19-rt-arm64</li>
  <li>linux-image-4.19-rt-armmp</li>
</ul>

<p>For example, if the command "uname -r" currently shows
"4.19.0-0.bpo.9-amd64", you should install linux-image-4.19-amd64.</p>

<p>There is no need to upgrade systems using Linux 4.9, as that kernel
version will also continue to be supported in the LTS period.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2324.data"
# $Id: $
