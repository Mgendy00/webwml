<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>keepalived has a heap-based buffer overflow when parsing HTTP status
codes resulting in DoS or possibly unspecified other impact, because
extract_status_code in lib/html.c has no validation of the status code
and instead writes an unlimited amount of data to the heap.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:1.2.13-1+deb8u1.</p>

<p>We recommend that you upgrade your keepalived packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1589.data"
# $Id: $
