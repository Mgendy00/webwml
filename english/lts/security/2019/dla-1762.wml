<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities have been addressed in the systemd components
systemd-tmpfiles and pam_systemd.so.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18078">CVE-2017-18078</a>

    <p>systemd-tmpfiles in systemd attempted to support ownership/permission
    changes on hardlinked files even if the fs.protected_hardlinks sysctl
    is turned off, which allowed local users to bypass intended access
    restrictions via vectors involving a hard link to a file for which
    the user lacked write access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3842">CVE-2019-3842</a>

    <p>It was discovered that pam_systemd did not properly sanitize the
    environment before using the XDG_SEAT variable. It was possible for
    an attacker, in some particular configurations, to set a XDG_SEAT
    environment variable which allowed for commands to be checked against
    polkit policies using the <q>allow_active</q> element rather than
    <q>allow_any</q>.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
215-17+deb8u12.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1762.data"
# $Id: $
