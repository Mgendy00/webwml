<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in SoX (Sound eXchange),
a sound processing program:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11332">CVE-2017-11332</a>

    <p>The startread function (wav.c) is affected by a divide-by-zero
    vulnerability when processing WAV file with zero channel count. This
    flaw might be leveraged by remote attackers using a crafted WAV file
    to perform denial of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11358">CVE-2017-11358</a>

    <p>The read_samples function (hcom.c) is affected by an invalid memory read
    vulnerability when processing HCOM files with invalid dictionnaries. This
    flaw might be leveraged by remote attackers using a crafted HCOM file to
    perform denial of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11359">CVE-2017-11359</a>

    <p>The wavwritehdr function (wav.c) is affected by a divide-by-zero
    vulnerability when processing WAV files with invalid channel count over
    16 bits. This flaw might be leveraged by remote attackers using a crafted
    WAV file to perform denial of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15371">CVE-2017-15371</a>

    <p>The sox_append_comment() function (formats.c) is vulnerable to a reachable
    assertion when processing FLAC files with metadata declaring more comments
    than provided. This flaw might be leveraged by remote attackers using
    crafted FLAC data to perform denial of service (application crash).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
14.4.1-5+deb8u3.</p>

<p>We recommend that you upgrade your sox packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1705.data"
# $Id: $
