<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In LibTIFF 4.0.8, there is a heap-based buffer overflow in the t2p_write_pdf
function in tools/tiff2pdf.c. This heap overflow could lead to different
damages. For example, a crafted TIFF document can lead to an out-of-bounds read
in TIFFCleanup, an invalid free in TIFFClose or t2p_free, memory corruption in
t2p_readwrite_pdf_image, or a double free in t2p_free. Given these
possibilities, it probably could cause arbitrary code execution.</p>

<p>This overflow is linked to an underlying assumption that all pages in a tiff
document will have the same transfer function. There is nothing in the tiff
standard that says this needs to be the case.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u17.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1206.data"
# $Id: $
