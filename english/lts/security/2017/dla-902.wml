<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in imagemagick that allow
remote attackers to cause a denial of service (application crash and
infinite loop) or possibly other unspecified impact via a crafted image.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
8:6.7.7.10-5+deb7u13.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-902.data"
# $Id: $
