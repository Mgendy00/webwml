<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in libav:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8365">CVE-2015-8365</a>

    <p>The smka_decode_frame function in libavcodec/smacker.c does not verify that
    the data size is consistent with the number of channels, which allows remote
    attackers to cause a denial of service (out-of-bounds array access) or
    possibly have unspecified other impact via crafted Smacker data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7208">CVE-2017-7208</a>

    <p>The decode_residual function in libavcodec allows remote attackers to cause
    a denial of service (buffer over-read) or obtain sensitive information from
    process memory via a crafted h264 video file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7862">CVE-2017-7862</a>

    <p>The decode_frame function in libavcodec/pictordec.c is vulnerable to an
    out-of-bounds write caused by a heap-based buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9992">CVE-2017-9992</a>

    <p>The decode_dds1 function in libavcodec/dfa.c allows remote attackers to
    cause a denial of service (Heap-based buffer overflow and application crash)
    or possibly have unspecified other impact via a crafted file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6:0.8.21-0+deb7u1.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1142.data"
# $Id: $
