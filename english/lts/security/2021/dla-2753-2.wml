<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that the patch for <a href="https://security-tracker.debian.org/tracker/CVE-2021-3592">CVE-2021-3592</a> introduced a regression which
prevented ssh connections to the host system. Since there is no imminent
solution for the problem, the patch for <a href="https://security-tracker.debian.org/tracker/CVE-2021-3592">CVE-2021-3592</a> has been reverted.
Updated qemu packages are now available to correct this issue.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:2.8+dfsg-6+deb9u16.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2753-2.data"
# $Id: $
