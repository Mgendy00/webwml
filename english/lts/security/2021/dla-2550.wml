<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various overflow errors were identified and fixed.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27814">CVE-2020-27814</a>

    <p>A heap-buffer overflow was found in the way openjpeg2 handled certain PNG format files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27823">CVE-2020-27823</a>

    <p>Wrong computation of x1,y1 if -d option is used, resulting in heap buffer
    overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27824">CVE-2020-27824</a>

    <p>Global buffer overflow on irreversible conversion when too many
    decomposition levels are specified.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27841">CVE-2020-27841</a>

    <p>Crafted input to be processed by the openjpeg encoder could cause an
    out-of-bounds read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27844">CVE-2020-27844</a>

    <p>Crafted input to be processed by the openjpeg encoder could cause an
    out-of-bounds write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27845">CVE-2020-27845</a>

    <p>Crafted input can cause out-of-bounds-read.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.1.2-1.1+deb9u6.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>For the detailed security status of openjpeg2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openjpeg2">https://security-tracker.debian.org/tracker/openjpeg2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2550.data"
# $Id: $
