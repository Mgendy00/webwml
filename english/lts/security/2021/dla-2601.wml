<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>cloud-init has the ability to generate and set a randomized password
for system users.  This functionality is enabled at runtime by
passing cloud-config data such as:</p>

   <p>chpasswd:
       list: |
           user1:RANDOM</p>

<p>When used this way, cloud-init logs the raw, unhashed password to a
world-readable local file.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.7.9-2+deb9u1.</p>

<p>We recommend that you upgrade your cloud-init packages.</p>

<p>For the detailed security status of cloud-init please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cloud-init">https://security-tracker.debian.org/tracker/cloud-init</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2601.data"
# $Id: $
