#use wml::debian::template title="Introduktion till Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"

<a id=community></a>
<h2>Debian är en gemenskap av folk</h2>
<p>Tusentals frivilliga runt hela världen arbetar tillsammans för använadares behov 
med Fri mjukvara som en prioritet.</p>


<ul>
  <li>
    <a href="people">Folk:</a>
    Vilka vi är, vad vi gör
  </li>
  <li>
    <a href="philosophy">Filosofi:</a>
    Varför vi gör det och hur vi gör det
  </li>
  <li>
    <a href="../devel/join/">Engagera dig:</a>
    Du kan bli en del av detta!
  </li>
  <li>
    <a href="help">Hur kan du hjälpa Debian?</a>
  </li>
  <li>
    <a href="../social_contract">Socialt Kontract:</a>
    Vår moraliska agenda
  </li>
  <li>
    <a href="diversity">Mångfaldserkännande</a>
  </li>
  <li>
    <a href="../code_of_conduct">Uppförandekod</a>
  </li>
  <li>
    <a href="../partners/">Partners:</a>
    Företag och organisationer som tillhandahåller pågående assistans
    till Debianprojektet
  </li>
  <li>
    <a href="../donations">Donationer</a>
  </li>
  <li>
    <a href="../legal/">Rättsliga frågor</a>
  </li>
  <li>
    <a href="../legal/privacy">Dataintegritet</a>
  </li>
  <li>
    <a href="../contact">Kontakta oss</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian är ett fritt operativsystem</h2>
<p>Vi börjar med Linux och lägger till många tusen mjukvaror för att tillfresställa
användarens behov.</p>

<ul>
  <li>
    <a href="../distrib">Hämta:</a>
    Flera varianter av Debianavbildningar
  </li>
  <li>
  <a href="why_debian">Varför Debian</a>
  </li>
  <li>
    <a href="../support">Support:</a>
    Få hjälp
  </li>
  <li>
    <a href="../security">Säkerhet:</a>
    Senaste uppdatering <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> Mjukvarupaket:</a>
    Sök och bläddra i den långa listan på vår mjukvara
  </li>
  <li>
    <a href="../doc"> Dokumentation</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Debianwikin</a>
  </li>
  <li>
    <a href="../Bugs"> Felrapporter</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Sändlistor</a>
  </li>
  <li>
    <a href="../blends"> Pure Blends:</a>
    Metapaket för speciella behov
  </li>
  <li>
    <a href="../devel"> Utvecklarhörnet:</a>
    Information primärt av intresse för Debianutvecklare
  </li>
  <li>
    <a href="../ports"> Anpassningar/Arkitekturer:</a>
    CPU-arkitekturer som vi stödjer
  </li>
  <li>
    <a href="search">Information om hur du använder Debians sökmotor</a>.
  </li>
  <li>
    <a href="cn">Information om sidor som finns tillgängliga på flera språk</a>.
  </li>
</ul>
