#use wml::debian::template title="Debian på skrivbordet"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896"

<h2>Det universella operativsystemet som ditt skrivbord</h2>

<p>
 Delprojektet Debian Desktop är en grupp frivilliga som vill skapa det bästa
 möjliga operativsystemet för användning i hemmet och på arbetsstationer i
 företag.
 Vårt motto är <q>Programvara som bara fungerar</q>.
 Kortfattat är vårt mål att få in Debian, GNU och Linux i
 &rdquo;<span lang="en">mainstream</span>&rdquo;-världen.
 <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop"/>
</p>

<h3>Våra grundsatser</h3>

<ul>
 <li>
  Vi erkänner att många <a href="https://wiki.debian.org/DesktopEnvironment">skrivbordsmiljöer</a>
  existerar, och kommer att stödja dess användning, och säkerställa att de
  fungerar bra på Debian.
 </li>
 <li>
  Vi ser att det bara finns två sorters användare som räknas:
  nybörjaren och experten.
  Vi kommer göra allt som är möjligt för att göra saker enkla för nybörjaren,
  samtidigt som det låter experterna att ställa in saker om de vill.
 </li>
 <li>
  Vi kommer försöka se till att programvara konfigureras för den vanligaste
  skrivbordsanvändningen.
  Till exempel bör det vanliga användarkonto som skapas automatiskt vid
  installationen ha tillgång till att spela ljud, skriva ut och hantera systemet
  via sudo.
 </li>
 <li>
  <p>
  Vi kommer försöka att se till att de frågor som ställs till användaren (vilka 
  bör hållas så få som möjligt) går att förstå även med ett minimum av 
  datorkunskap. Vissa Debianpaket presenterar idag avancerade tekniska detaljer
  för användaren. Tekniska debconf-frågor som presenteras för användaren av
  Debianinstalleraren bör undvikas. För en nybörjare är detta endast skrämmande
  och förvirrande. För en expert kan de vara irriterande och onödiga. En
  nybörjare kanske inte ens vet vad frågorna handlar om. En expert kan
  konfigurera deras skrivbordsmiljö hur de vill efter att installationen är
  färdig. Prioriteten för dylika Debconffrågor bör åtminstone sänkas.
  </p>
 </li>
 <li>
  Och vi ska ha kul medan vi gör allt detta!
 </li>
</ul>

<h3>Hur du kan hjälpa till</h3>

<p>
 Det viktigaste av ett delprojekt inom Debian är inte sändlistorna, webbsidorna
 eller arkivutrymme för paketen.
 Den viktigaste ingrediensen är <em>motiverade människor</em> som kan få saker
 att hända.
 Du behöver inte vara en officiell utvecklare för att börja tillverka paket och
 patchar, Debian Desktops kärngrupp kommer se till att ditt arbete integreras.
 Här är några av sakerna du kan göra:
</p>

<ul>
 <li>
  Testa vår funktion <q>skrivbordsstandardmiljö</q> (eller funktionen
  kde-desktop), installera en av våra
  <a href="$(DEVEL)/debian-installer/">testavbildningar för nästa utgåva</a>
  och sända kommentarer till
  <a href="https://lists.debian.org/debian-desktop/">sändlistan
  debian-desktop</a>.
 </li>
 <li>
  Arbeta på <a href="$(DEVEL)/debian-installer/">debian-installer</a>.
  GTK+-skalet behöver dig.
 </li>
 <li>
  Hjälpa <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME-gruppen</a>,
  <a href="https://qt-kde-team.pages.debian.net/">Debian Qt- och KDE-grupperna</a> eller
  <a href="https://salsa.debian.org/xfce-team/">Debian Xfce-gruppen</a>.
  Du kan hjälpa till med paketering, felbedömning, dokumentation, testning och
  mer.
 </li>
 <li>
  Lära användare hur man installerar och använder de skrivbordsfunktioner vi 
  nu har i Debian (desktop, gnome-desktop och kde-desktop).
 </li>
 <li>
  Arbeta på att sänka prioriteten på, eller ta bort onödiga
  <a href="https://packages.debian.org/debconf">debconf</a>-frågor från paket,
  och göra de som behövs lättare att förstå.
 </li>
 <li>
  Hjälpa
  <a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian Desktop
  Artwork-projektet</a>.
 </li>
</ul>

<h3>Wiki</h3>

<p>
 Vi har några artiklar på vår wiki, och våran ingångssida där är
 <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>.
 Några av Debian Desktops wikiartiklar är föråldrade.
</p>

<h3>Sändlista</h3>
 
<p>
 Underprojektet diskuteras på sändlistan
 <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
</p>

<h3>IRC-kanal</h3>

<p>
 Vi uppmuntrar alla (Debianutvecklare eller inte) som är intresserade av Debian
 Desktop att gå med på #debian-desktop på
 <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org).
</p>

<h3>Inblandade personer</h3>

<p>
 Alla som vill är välkomna.
 Faktum är att alla i pkg-gnome-, pkg-kde- och pkg-xfce-grupperna är
 indirekt inblandade.
 Sändlistan debian-desktop:s prenumeranter är aktiva bidragslämnare.
 Debian-installer- och tasksel-grupperna är också viktiga för våra
 målsättningar.
</p>

<p>
 Denna sida underhålls av
 <a href="https://people.debian.org/~stratus/">Gustavo Franco</a>.
 Den underhölls tidigare av
 <a href="https://people.debian.org/~madkiss/">Martin Loschwitz</a> och
 <a href="https://people.debian.org/~walters/">Colin Walters</a>.
</p>
