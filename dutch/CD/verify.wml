#use wml::debian::cdimage title="De authenticiteit van Debian cd's controleren" BARETITLE=true
#use wml::debian::translation-check translation="553c55e0ca1d5d45ffeaab254ac5430bed284238"

<p>
Officiële releases van Debian cd's bevatten een ondertekend checksum-bestand.
U vindt dit samen met het image in de map <code>iso-cd</code>,
<code>jigdo-dvd</code>, <code>iso-hybrid</code>, enz.
Dit laat u toe na te gaan of de images welke u downloadt, correct zijn.
Vooreerst kan de checksum gebruikt worden om te controleren of het cd-image
bij het downloaden niet beschadigd werd.
Bovendien bevestigt de ondertekening van het checksum-bestand dat de bestanden
wel degelijk deze zijn welke officieel uitgegeven werden door het
team van Debian CD / Debian Live, en dat er niet mee geknoeid werd.
</p>

<p>
Om zekerheid te verkrijgen over de geldigheid van de inhoud van een cd-image,
moet u het juiste checksum-gereedschap gebruiken.
Voor elke release wordt met cryptografisch sterke algoritmes
(SHA256 and SHA512) gewerkt. Om daarmee bewerkingen uit te voeren, moet u
gebruik maken van de hulpmiddelen <code>sha256sum</code> en <code>sha512sum</code>.
</p>

<p>
Om er zeker van te zijn dat de checksum-bestanden zelf correct zijn,
moet u GnuPG gebruiken om ze te verifiëren aan de hand van het begeleidende
ondertekeningsbestand (bijv. <code>SHA512SUMS.sign</code>).
De sleutels welke gebruikt worden voor deze ondertekening bevinden zich
allemaal in de <a href="https://keyring.debian.org">Debian GPG-sleutelbos</a>,
en de beste manier om deze te controleren, is deze sleutelbos te gebruiken om
deze via het web van vertrouwen (the web of trust) geldig te laten verklaren.
Om het gebruikers gemakkelijker te maken, volgen hieronder de vingerafdrukken
van de sleutels die de laatste jaren voor releases gebruikt werden:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
